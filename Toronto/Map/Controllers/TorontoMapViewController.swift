//
//  TorontoMapViewController.swift
//  Toronto
//
//  Created by Eric Williams on 2018-01-20.
//  Copyright © 2018 Eric Williams. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class TorontoMapViewController: BaseViewController {
    
    // IBOutlets
    @IBOutlet weak var mapView: MKMapView!
    
    // MARK: - ViewController LifeCycle:
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .followWithHeading
        mapView.mapType = .mutedStandard
        mapView.showsBuildings = true
        mapView.showsCompass = false
        
        setupMapViewData()
        setupCoreLocationService()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkLocationAuthorizationStatus()
    }
    
    // MARK: - Setup DataSource
    var allEventsArray =  [FestivalEventModel]()
    var featuredEventsArray = [FestivalEventModel]()
    var eventMapMarkers = [EventMapMarker]()
    
    // MARK: - CoreLocation Service Setup:
    let locationManager = CLLocationManager()
    let kDefaultLocation = CLLocation(latitude: 43.653908, longitude: -79.384293)
    let regionRadius: CLLocationDistance = 3800
    
    fileprivate func setupCoreLocationService() {
        locationManager.delegate = self
        
        // If user location is not enable start at Toronto City Hall
        if let coordinate = locationManager.location?.coordinate {
            let userLocation = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
            centerMapOnLocation(location: userLocation)
        } else {
            centerMapOnLocation(location: kDefaultLocation)
        }
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
    fileprivate func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        } else {
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
}

extension TorontoMapViewController {
    fileprivate func setupMapViewData() {
        Networking.sharedInstance.fetchTorontoFestivalsEvents(success: { festivalEvent in
            _ = festivalEvent.map {
                
                // Add all Festival Events to: allEventsArray
                self.allEventsArray.append($0)
                
                // Add only Festival Events with Images to: featuredEventsArray
                if $0.calEvent?.image?.url != nil && $0.calEvent?.shortDescription != nil {
                    self.featuredEventsArray.append($0)
                }
                
                if let event = $0.calEvent {
                    //                    let eventMarker = EventMapMarker(eventName: event.eventName ?? "", coordinate: CLLocationCoordinate2DMake((event.locations?.first?.coords?.lat) ?? 0.0, (event.locations?.first?.coords?.lng) ?? 0.0), shortDescription: event.description ?? "")
                    var lat = 0.0
                    var lng = 0.0
                    
                    if let qCoords = $0.calEvent?.locations?.first?.coords {
                        switch qCoords {
                        case .coords(let x):
                            print("\n\n.coords: \(qCoords)")
                            lat = x.lat ?? 0.0
                            lng = x.lng ?? 0.0
                        case .coordsArray(_):
                            print("\n\n.coordsArray \($0.calEvent?.locations?.first?.locationName): \(qCoords) \n ---Ends\n\n\n")
                        }
                    }
                    
                    let coordinate = CLLocationCoordinate2DMake(lat, lng)
                    
                    let eventMarker = EventMapMarker(eventName: event.eventName ?? "-",
                                                     coordinate: coordinate,
                                                     shortDescription: event.description ?? "-")
                    
                    self.mapView.addAnnotation(eventMarker)
                }
                
            }
            
        }) { (oops) in
            if let oops = oops {
                print(oops.localizedDescription)
            }
        }
    }
}

extension TorontoMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? EventMapMarker else { return nil }

        let identifier = "eventMapMarker"
        var view: MKMarkerAnnotationView

        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = createRightCalloutAccessoryView()//UIButton(type: .detailDisclosure)
        }
        return view
    }
    
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
         mapView.layoutIfNeeded()
    }
    
    // MARK: - Create Custom Callout Views
    func createRightCalloutAccessoryView() -> UIButton {
        let rightCalloutAccessoryButton = UIButton(frame: CGRect(origin: .zero, size: CGSize(width: 30, height: 30)))
        rightCalloutAccessoryButton.setBackgroundImage(UIImage(named:"ic_compass_needle"), for: UIControlState())
        return rightCalloutAccessoryButton
    }
}

extension TorontoMapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationValue: CLLocationCoordinate2D = manager.location!.coordinate
        print("did update locations: \(locationValue)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("did fail with error: unable to retrieve a location value")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        mapView.showsUserLocation = (status == .authorizedAlways)
    }
    
//    func locationManager(_ manager: CLLocationManager, didUpdateTo: CLLocation, from: CLLocation) {
//
//    } /* Deprecated? */
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print("did enter")
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("did exit")
    }
}

