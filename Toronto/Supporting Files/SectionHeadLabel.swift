//
//  SectionHeadLabel.swift
//  Toronto
//
//  Created by Eric Williams on 2018-11-07.
//  Copyright © 2018 Eric Williams. All rights reserved.
//

import UIKit

class SectionHeadLabel: UILabel {

    let padding = UIEdgeInsetsMake(40, 8, 16, 8)
    override func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, padding))
    }

    override var intrinsicContentSize : CGSize {
        let superContentSize = super.intrinsicContentSize
        let width = superContentSize.width + padding.left + padding.right
        let heigth = superContentSize.height + padding.top + padding.bottom
        return CGSize(width: width, height: heigth)
    }
    
}
