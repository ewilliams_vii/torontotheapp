//
//  BaseViewController.swift
//  Toronto
//
//  Created by Eric Williams on 2018-01-21.
//  Copyright © 2018 Eric Williams. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // ToDo: Move navigation controller setup to base viewcontroller class:
        if let customNavigationBar = navigationController?.navigationBar {
            let emptyUIImage = UIImage()
            customNavigationBar.setBackgroundImage(emptyUIImage, for: UIBarMetrics.default)
            customNavigationBar.shadowImage = emptyUIImage
            customNavigationBar.barStyle = .default
            customNavigationBar.tintColor = UIColor.darkText
            customNavigationBar.topItem?.title = "The Toronto App"
            customNavigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.darkGray,
                                                       NSAttributedStringKey.font: UIFont(name: "ArialRoundedMTBold", size: 21)!]
            //            reference: https://goo.gl/4PWFwF
            //            navigationItem.title = "torontOH!"
            //            navigationItem.titleView = UIView()
            
        }
    }

}
