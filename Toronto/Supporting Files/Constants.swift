//
//  Constants.swift
//  Toronto
//
//  Created by Eric Williams on 2018-01-01.
//  Copyright © 2018 Eric Williams. All rights reserved.
//

import Foundation
// MARK: - googlw api key:
//AIzaSyCFlHNwcnOL0TC6qwPKxFPnlx0sEHBgoyA

// MARK: - User Default Keys
struct UserDefaultKeys {
    static let onboarComplete = "OnboardComplete"
}


public struct Constants {
    static let bundle = Bundle(identifier: "co.ericwilliams.Toronto")
}

// MARK: - NSNotification Center Setup
let nc = NotificationCenter.default

// MARK: - Font Sizes
struct FontSizes {
    static let x_large: Float = 21.0
    static let large: Float = 14.0
    static let small: Float = 10.0
}

// MARK: - TableView Cell Identifiers
struct CellIdentifiers {
    static let WaiterShiftTable = "waiterShiftTableViewCell"
}

// MARK: - Festival Events 
struct NetworkCall {
    static let torontoFestivalEvents_first20 = URL(string: "http://app.toronto.ca/cc_sr_v1_app/data/edc_eventcal_APR?start=1&limit=20")
    static let torontoFestivalEvents_first21to40 = URL(string: "http://app.toronto.ca/cc_sr_v1_app/data/edc_eventcal_APR?start=21&limit=20")
    static let torontoFestivalEvents_first100 = URL(string: "http://app.toronto.ca/cc_sr_v1_app/data/edc_eventcal_APR?limit=100")
    static let torontoFestivalEvents_first500 = URL(string: "http://app.toronto.ca/cc_sr_v1_app/data/edc_eventcal_APR?limit=500")
}

struct BaseUrl {
    static let torontoFestivalEventsImagesEndpoint = URL(string:"https://secure.toronto.ca")
}  
