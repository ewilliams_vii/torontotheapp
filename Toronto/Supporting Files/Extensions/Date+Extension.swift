//
//  Date+Extension.swift
//  Toronto
//
//  Created by Eric Williams on 2018-09-17.
//  Copyright © 2018 Eric Williams. All rights reserved.
//

import Foundation

public extension Date {
    struct Formatter {
        static let ddMMMyyyy: DateFormatter = {
            let formatter = DateFormatter()
            formatter.calendar = Calendar(identifier: .gregorian)
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "dd MMM, yyyy"
            return formatter
        }()
        
        static let screenReadable: DateFormatter = {
            let formatter = DateFormatter()
            formatter.calendar = Calendar(identifier: .gregorian)
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "dd MMM, yyyy HH:MMa"
            return formatter
        }()
        
        static let screenReadableShort: DateFormatter = {
            let formatter = DateFormatter()
            formatter.calendar = Calendar(identifier: .gregorian)
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "dd MMM, yyyy"
            return formatter
        }()
        
        static let screenReadableTime: DateFormatter = {
           let formatter = DateFormatter()
            formatter.calendar = Calendar(identifier: .gregorian)
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "HH:MMa"
            return formatter
        }()
    }
    
    var ddMMMyyyy: String {
        return Formatter.ddMMMyyyy.string(from: self)
    }
    
    var screenReadable: String {
        return Formatter.screenReadable.string(from: self)
    }
    
    var screenReadableShort: String {
        return Formatter.screenReadableShort.string(from: self)
    }
    
    var screenReadableTime: String {
        return Formatter.screenReadableTime.string(from: self)
    }
}
