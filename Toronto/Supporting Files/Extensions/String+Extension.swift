//
//  String+Extension.swift
//  Toronto
//
//  Created by Eric Williams on 2018-09-17.
//  Copyright © 2018 Eric Williams. All rights reserved.
//

import Foundation

public extension String {
    init?(htmlEncodedString: String) {
        
        guard let data = htmlEncodedString.data(using: .utf8) else {
            return nil
        }
        
        let options: [NSAttributedString.DocumentReadingOptionKey : Any] = [
            NSAttributedString.DocumentReadingOptionKey(rawValue: NSAttributedString.DocumentAttributeKey.documentType.rawValue): NSAttributedString.DocumentType.html,
            NSAttributedString.DocumentReadingOptionKey(rawValue: NSAttributedString.DocumentAttributeKey.characterEncoding.rawValue): String.Encoding.utf8.rawValue
        ]
        
        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
            return nil
        }
        
        self.init(attributedString.string)
    }
    
    var iso8601StringToDate: Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        return formatter.date(from: self)
    }
    
    var dd_mm_yyyy: Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        return formatter.date(from: self)
    }
    
    func convertToScreenReadable() -> String {
        guard let date = self.iso8601StringToDate else { return "-" }
        let screenReadableDate = date.screenReadable
        return screenReadableDate
    }
    
    func convertToScreenReadableShort() -> String {
        guard let date = self.iso8601StringToDate else { return "-" }
        let screenReadableDate = date.screenReadableShort
        return screenReadableDate
    }
}
