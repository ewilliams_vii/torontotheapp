//
//  UIImageView+Extension.swift
//  Toronto
//
//  Created by Eric Williams on 2018-01-09.
//  Copyright © 2018 Eric Williams. All rights reserved.
//

import UIKit

extension UIImageView {
    
    var storedProperty: String {
        get {
            print("something")
            return "something to return"
        }
        
        set {
            print("something else")
        }
    }
    var computedProperty: String {
        return "should work?"
    }
    
    fileprivate func getDataFromUrl(url: URL, completion:@escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, oops in
            completion(data, response, oops)
        }.resume()
    }
    
    func downloadImage(for url: URL, contentMode: UIViewContentMode? = .scaleToFill) {
        getDataFromUrl(url: url) { (data, response, oops) in
            guard let data = data, oops == nil else {
                print("UIImageView+Extension.swift > downloadImage(for url: URL) > getDataFromUrl(url: url) - Failed")
                return
            }
            
            DispatchQueue.main.async(execute: {
                guard let contentMode = contentMode else { return }
                self.contentMode = contentMode
                self.image = UIImage(data: data)
            })
        }
    }
    
}
