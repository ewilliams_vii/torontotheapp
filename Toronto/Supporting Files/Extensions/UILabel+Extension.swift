//
//  UILabel+Extension.swift
//  Toronto
//
//  Created by Eric Williams on 2018-01-09.
//  Copyright © 2018 Eric Williams. All rights reserved.
//

import UIKit

extension UILabel {
    
    func setDefaultStyle() {
        self.numberOfLines = 0
        self.font = UIFont.systemFont(ofSize: 17)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 8.5
        
        guard let labelCopy = self.text else { return }
        let attrString = NSMutableAttributedString(string: labelCopy)
        attrString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attrString.length))
        
        self.attributedText = attrString
        self.textAlignment = NSTextAlignment.left
    }
}

