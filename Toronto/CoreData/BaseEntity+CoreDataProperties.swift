//
//  BaseEntity+CoreDataProperties.swift
//  
//
//  Created by Eric Williams on 2019-08-18.
//
//

import Foundation
import CoreData


extension BaseEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BaseEntity> {
        return NSFetchRequest<BaseEntity>(entityName: "BaseEntity")
    }

    @NSManaged public var id: String?
    @NSManaged public var idRemote: String?
    @NSManaged public var syncState: String?
    @NSManaged public var updated_: NSDate?
    @NSManaged public var weight: Int16

}
