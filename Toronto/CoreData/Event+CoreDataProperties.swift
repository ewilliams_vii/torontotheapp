//
//  Event+CoreDataProperties.swift
//  
//
//  Created by Eric Williams on 2019-08-18.
//
//

import Foundation
import CoreData


extension Event {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Event> {
        return NSFetchRequest<Event>(entityName: "Event")
    }

    @NSManaged public var admissionCosts: NSObject?
    @NSManaged public var admissionPrice: String?
    @NSManaged public var categories: NSObject?
    @NSManaged public var date: NSDate?
    @NSManaged public var descriptionShort: String?
    @NSManaged public var endDate: String?
    @NSManaged public var endTime: String?
    @NSManaged public var imageAltText: String?
    @NSManaged public var imageFileSize: Int16
    @NSManaged public var imageFileType: String?
    @NSManaged public var imageHeight: Int16
    @NSManaged public var imageName: String?
    @NSManaged public var imageURI: URL?
    @NSManaged public var imageWidth: Int16
    @NSManaged public var isAllDay: Bool
    @NSManaged public var isCancelled: Bool
    @NSManaged public var isFeatured: Bool
    @NSManaged public var isFree: Bool
    @NSManaged public var isOutdoors: Bool
    @NSManaged public var isRescheduled: Bool
    @NSManaged public var isSaved: Bool
    @NSManaged public var locations: NSObject?
    @NSManaged public var name: String?
    @NSManaged public var organizers: String?
    @NSManaged public var rescheduledDate: NSDate?
    @NSManaged public var reservationEmail: String?
    @NSManaged public var reservationPhone: Int16
    @NSManaged public var reservationWebsite: URL?
    @NSManaged public var sharedWith: NSObject?
    @NSManaged public var thumbnailAltText: String?
    @NSManaged public var thumbnailFileSize: Int16
    @NSManaged public var thumbnailFileType: String?
    @NSManaged public var thumbnailHeight: Int16
    @NSManaged public var thumbnailName: String?
    @NSManaged public var thumbnailURI: URL?
    @NSManaged public var thumbnailWidth: Int16
    @NSManaged public var website: URL?
    @NSManaged public var descriptionLong: String?
    @NSManaged public var startTime: String?
    @NSManaged public var startDate: String?
    @NSManaged public var location: NSSet?

}

// MARK: Generated accessors for location
extension Event {

    @objc(addLocationObject:)
    @NSManaged public func addToLocation(_ value: Location)

    @objc(removeLocationObject:)
    @NSManaged public func removeFromLocation(_ value: Location)

    @objc(addLocation:)
    @NSManaged public func addToLocation(_ values: NSSet)

    @objc(removeLocation:)
    @NSManaged public func removeFromLocation(_ values: NSSet)

}
