//
//  Location+CoreDataProperties.swift
//  
//
//  Created by Eric Williams on 2019-08-18.
//
//

import Foundation
import CoreData


extension Location {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Location> {
        return NSFetchRequest<Location>(entityName: "Location")
    }

    @NSManaged public var address: String?
    @NSManaged public var coords: NSObject?
    @NSManaged public var geoCoded: Bool
    @NSManaged public var name: String?
    @NSManaged public var type: String?
    @NSManaged public var event: NSSet?

}

// MARK: Generated accessors for event
extension Location {

    @objc(addEventObject:)
    @NSManaged public func addToEvent(_ value: Event)

    @objc(removeEventObject:)
    @NSManaged public func removeFromEvent(_ value: Event)

    @objc(addEvent:)
    @NSManaged public func addToEvent(_ values: NSSet)

    @objc(removeEvent:)
    @NSManaged public func removeFromEvent(_ values: NSSet)

}
