//
//  EventMapMarker.swift
//  Toronto
//
//  Created by Eric Williams on 2018-01-20.
//  Copyright © 2018 Eric Williams. All rights reserved.
//

import UIKit
import MapKit

class EventMapMarker: NSObject, MKAnnotation {
    var title: String?
    var coordinate: CLLocationCoordinate2D
    var shortDescription: String?
//    var thumbImage: ThumbImage?
 
    var subtitle: String? {
        return shortDescription
    }
    
    init(eventName: String = "", coordinate: CLLocationCoordinate2D, shortDescription: String = "") { //, thumbImage: ThumbImage) {
        self.title = eventName
        self.coordinate = coordinate
        self.shortDescription = shortDescription
//        self.thumbImage = thumbImage
    }
    
}
