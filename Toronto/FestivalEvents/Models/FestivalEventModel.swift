//
//  FestivalEventModel.swift
//  Toronto
//
//  Created by Eric Williams on 2018-01-01.
//  Copyright © 2018 Eric Williams. All rights reserved.
//

import Foundation 

struct FestivalEventModel: Decodable {
    let calEvent: CalEvent?
}

struct CalEvent: Decodable {
    let orgType: String?
    let startDate: String?
    let thumbImage: ThumbImage?
    let startDateTime: String?
    let timeInfo: String?
    let weeklyDates: [WeeklyDates]?
    let orgAddress: String?
    let terms: String?
    let endDate: String?
    let frequency: String?
    let partnerType: String?
    let eventWebsite: String?
    let orgPhone: String?
    let dates: [Dates]?
    let locations: [Locations]?
    let description: String?
    let otherCostInfo: String?
    let features: Features?
    let categoryString: String?
    let themeString: String?
    let accessibility: String?
    let allDay: Bool?
    let image: EventImage?
    let reservationsRequired: String?
    let orgName: String?
    let cost: EventCost?
    let endDateTime: String?
    let category: [EventCategory]?
    let eventPhone: String?
    let shortDescription: String?
    let freeEvent: String?
    let eventName: String?
}

struct ThumbImage: Decodable {
    let fileSize: Int?
    let fileType: String?
    let binId: String?
    let fileName: String?
    let url:  String?
}

struct WeeklyDates: Decodable {
    let startTime: String?
    let weekDay: [WeekDay]?
    let endTime: String?
}

struct WeekDay: Decodable {
    let day: String?
}

struct Dates: Decodable {
    let startDateTime: String?
    let allDay: Bool?
    let endDateTime: String?
}

struct Locations: Decodable {
    let id: String?
//    let coords: Coords? // could be [Coords]?
    let coords: QCoords
    let address: String?
    let geoCoded: Bool?
    let locationType: [LocationType]?
    let locationName: String?
    let type: String? 
}

struct LocationType: Decodable {
    let type: String?
}

struct Features: Decodable {
    let publicWashrooms: Bool?
    
    enum CodingKeys: String, CodingKey {
        case publicWashrooms = "Public Washrooms"
    }
}

struct EventImage: Decodable {
    let fileSize: Int?
    let fileType: String?
    let binId: String?
    let fileName: String?
    let altText: String?
    let url: String?
}

struct EventCost: Decodable {
    let child: Double?
    let student: Double?
    let youth: Double?
    let adult: Double?
    let senior: Double?
}

struct EventCategory: Decodable {
    let name: String?
}

struct Coords: Decodable {
    let lng: Double?
    let lat: Double?
}

enum QCoords: Decodable {
    case coords(Coords), coordsArray([Coords])
    init(from decoder: Decoder) throws {
        if let coords = try? decoder.singleValueContainer().decode(Coords.self) {
            self = .coords(coords)
            return
        }
        
        if let coordsArray = try? decoder.singleValueContainer().decode([Coords].self) {
            self = .coordsArray(coordsArray)
            return
        }
        
        throw QError.missingValue
    }
    
    enum QError: Error {
        case missingValue
    }
}

extension FestivalEventModel: Equatable {
    static func ==(lhs: FestivalEventModel, rhs: FestivalEventModel) -> Bool {
        return rhs.calEvent?.eventName == lhs.calEvent?.eventName
    }
}


