//
//  TorontoFestivalEventsWorker.swift
//  Toronto
//
//  Created by Eric Williams on 2019-08-14.
//  Copyright © 2019 Eric Williams. All rights reserved.
//

import Foundation
import CoreData

struct StartEndTime {
    var startDateTime: Date
    var allDay: Bool = false
    var endDateTime: Date
}

class TorontoFestivalEventsWorker {
    enum FilterResult {
        case success([FestivalEventModel])
        case failure(Error)
    }
    
    enum DateSource {
        case weeklyDates
        case dates
    }
    
    enum Frequency {
        case once
        case dates
        case weekly
    }
    
    private var events: [Event] = []
    
    var persistenceContainer = PersistenceService.sharedInstance.persistentContainer
    var context: NSManagedObjectContext {
        return persistenceContainer.viewContext
    }
    
    func start() {
        Networking.sharedInstance.fetchTorontoFestivalsEvents(success: { festivalEvents in
            
            for event in festivalEvents {
                guard let calEvent = event.calEvent else { return }
                guard let frequencey = calEvent.frequency else { return }
                switch frequencey {
                case "once":
                    if let startDateTime = calEvent.dates?.first?.startDateTime, let endDateTime = calEvent.dates?.first?.endDateTime {
                        let date = Dates(startDateTime: startDateTime, allDay: calEvent.dates?.first?.allDay ?? false, endDateTime: endDateTime)
                        self.locallyPersist(event: calEvent, date: date, weeklyDates: nil)
                    }
                case "dates":
                    self.handleRecurringEvents(event: calEvent, ocurring: .dates)
                case "weekly":
                    self.handleRecurringEvents(event: calEvent, ocurring: .weekly)
                default:
                    print("default")
                }
                PersistenceService.sharedInstance.saveContext()
            }
            
        }) { (oops) in
            if let oops = oops {
                print(oops.localizedDescription)
            }
        }
    }
    
    
    private func handleRecurringEvents(event calEvent: CalEvent, ocurring: Frequency) {
        if case Frequency.dates = ocurring {
            guard let datesArray = calEvent.dates else { return }
            for date in datesArray {
                if let startDateTime = date.startDateTime, let endDateTime = date.endDateTime {
                    let date = Dates(startDateTime: startDateTime, allDay: date.allDay ?? false, endDateTime: endDateTime)
                    locallyPersist(event: calEvent, date: date, weeklyDates: nil)
                }
            }
        }
        
        if case Frequency.weekly = ocurring {
            guard let weeklyDatesArray = calEvent.weeklyDates else { return }
            for weekday in weeklyDatesArray {
                if let startTime = weekday.startTime, let endTime = weekday.endTime, let weekDayArray = weekday.weekDay {
                    for day in weekDayArray {
                        let weeklyDates = WeeklyDates(startTime: startTime, weekDay: [day], endTime: endTime)
                        locallyPersist(event: calEvent, date: nil, weeklyDates: weeklyDates) 
                    }
                }
            }
        }
    }
    
    private func locallyPersist(event calEvent: CalEvent, date: Dates?, weeklyDates: WeeklyDates?) {
        let request: NSFetchRequest<Event> = Event.fetchRequest()
        let testIdRemote = getUniqueId(event: calEvent, date: date, weeklyDates: weeklyDates)
        request.predicate = NSPredicate(format: "idRemote = %@", testIdRemote ?? "")
        
        do {
            let matches = try context.fetch(request)
            if matches.count > 0 {
                print("matched") // send notification out to update views*
                return
            }
        } catch {
            print("error: \(error)")
        }
        
        guard let entity = NSEntityDescription.entity(forEntityName: "Event", in: context) else { fatalError("Could not find entity description") }
 
        let newEvent = NSManagedObject(entity: entity, insertInto: context)
        newEvent.setValue(UUID().uuidString, forKey: "id")
        
        
        let uniqueId = getUniqueId(event: calEvent, date: date, weeklyDates: weeklyDates)
        newEvent.setValue(uniqueId, forKey: "idRemote")
        print("new record added")
        
        newEvent.setValue(Date(), forKey: "updated_")
        
        newEvent.setValue(calEvent.eventName, forKey: "name")
        newEvent.setValue(calEvent.orgName, forKey: "organizers")
        if let eventWebsiteUrl = calEvent.eventWebsite {
            newEvent.setValue(URL(string: eventWebsiteUrl), forKey: "website")
        }
        
        newEvent.setValue(calEvent.shortDescription, forKey: "descriptionShort")
        newEvent.setValue(calEvent.description, forKey: "descriptionLong")
        newEvent.setValue(getCategories(calEvent), forKey: "categories")
        
        // Cost Info:
        newEvent.setValue(calEvent.otherCostInfo, forKey: "admissionPrice")
        if let cost = calEvent.cost {
            var costDictionary: [String: Double] = [:]
            
            let child = cost.child ?? -1.0
            let student = cost.student ?? -1.0
            let youth = cost.youth ?? -1.0
            let adult = cost.adult ?? -1.0
            let senior = cost.senior ?? -1.0
            
            costDictionary = ["Child": child]
            costDictionary = ["Student": student]
            costDictionary = ["Youth": youth]
            costDictionary = ["Adult": adult]
            costDictionary = ["Senior": senior]
            
            newEvent.setValue(costDictionary, forKey: "admissionCosts")
        }
        
        // Event Thumbnail Images:
        if let thumbnailImage = calEvent.thumbImage,
            let url = thumbnailImage.url,
            let thumbnailImageUrl = BaseUrl.torontoFestivalEventsImagesEndpoint?.appendingPathComponent(url),
            let fileSize = thumbnailImage.fileSize,
            let fileName = thumbnailImage.fileName,
            let binId = thumbnailImage.binId,
            let fileType = thumbnailImage.fileType {
            newEvent.setValue(fileName, forKey: "thumbnailName")
            newEvent.setValue(thumbnailImageUrl, forKey: "thumbnailURI")
            newEvent.setValue(fileType, forKey: "thumbnailFileType")
            newEvent.setValue(Int64(fileSize), forKey: "thumbnailFileSize")
            newEvent.setValue(binId, forKey: "thumbnailAltText")
            newEvent.setValue(0, forKey: "thumbnailWidth")
            newEvent.setValue(0, forKey: "thumbnailHeight")
        }
        
        // Event Images:
        if let image = calEvent.image,
            let url = image.url,
            let imageUrl = BaseUrl.torontoFestivalEventsImagesEndpoint?.appendingPathComponent(url),
            let fileSize = image.fileSize,
            let fileType = image.fileType,
            let fileName = image.fileName,
            let altText = image.altText,
            let binId = image.binId  {
            newEvent.setValue(fileName, forKey: "imageName")
            newEvent.setValue(imageUrl, forKey: "imageURI")
            newEvent.setValue(fileType, forKey: "imageFileType")
            newEvent.setValue(Int64(fileSize), forKey: "imageFileSize")
            newEvent.setValue(altText + " (" + binId + ")", forKey: "imageAltText")
            newEvent.setValue(0, forKey: "imageWidth")
            newEvent.setValue(0, forKey: "imageHeight")
        }
        
        // Date Info:
        if let date = date {
            newEvent.setValue(date.startDateTime?.iso8601StringToDate, forKey: "date")
            newEvent.setValue(date.allDay, forKey: "isAllDay")
            newEvent.setValue(date.startDateTime, forKey: "startDate")
            newEvent.setValue(date.endDateTime, forKey: "endDate")
            newEvent.setValue(date.startDateTime?.iso8601StringToDate?.screenReadableTime, forKey: "startTime")
            newEvent.setValue(date.endDateTime?.iso8601StringToDate?.screenReadableTime, forKey: "endTime")
        }
        
        if let weeklyDates = weeklyDates {
            newEvent.setValue(true, forKey: "isWeekly")
            newEvent.setValue(weeklyDates.startTime?.iso8601StringToDate?.screenReadable, forKey: "startTime")
            newEvent.setValue(weeklyDates.endTime?.iso8601StringToDate?.screenReadable, forKey: "endTime")
            if let dayOfWeek = weeklyDates.weekDay?.first?.day {
                newEvent.setValue(dayOfWeek, forKey: "dayOfWeek")
            }
        }
        
    }
}

extension TorontoFestivalEventsWorker {
    fileprivate func getUniqueId(event calEvent: CalEvent, date: Dates?, weeklyDates: WeeklyDates?) -> String? {
        if let date = date {
            if let eventName = calEvent.eventName, let startDateTime = date.startDateTime, let endDateTime = date.endDateTime {
                let uniqueIdStart = eventName + startDateTime + endDateTime
                let uniqueId = String(uniqueIdStart.filter { !" -:.&;".contains($0) }).lowercased()
                return uniqueId
            }
        } else if let weeklyDates = weeklyDates {
            if let eventName = calEvent.eventName, let startTime = weeklyDates.startTime, let endTime = weeklyDates.endTime, let dayOfWeek = weeklyDates.weekDay?.first?.day {
                let uniqueIdStart = eventName + startTime + endTime + dayOfWeek
                let uniqueId = String(uniqueIdStart.filter { !" -:.&;".contains($0) }).lowercased() 
                return uniqueId
            }
        }
        return nil
    }
}

extension TorontoFestivalEventsWorker {
    fileprivate func returnStartEndTime(from calEvent: CalEvent) -> [StartEndTime]? {
        var startEndTimes: [StartEndTime] = []
        guard let dates = calEvent.dates else { return startEndTimes }
        
        for date in dates {
            guard let startDateTime = date.startDateTime?.iso8601StringToDate, let endDateTime = date.endDateTime?.iso8601StringToDate else { return nil }
            let newStartEndTime = StartEndTime(startDateTime: startDateTime, allDay: date.allDay ?? false, endDateTime: endDateTime)
            startEndTimes.append(newStartEndTime)
        }
    
        return startEndTimes
    }
}

extension TorontoFestivalEventsWorker {
    fileprivate func hasLocations(_ calEvent: CalEvent) -> Bool {
        if let locations = calEvent.locations {
            return locations.count > 0
        } else {
            return false
        }
    }
    
    fileprivate func hasMultipleLocations(_ calEvent: CalEvent) -> Bool {
        guard let locations = calEvent.locations else { return false }
        return locations.count < 1
    }
}

extension TorontoFestivalEventsWorker {
    fileprivate func getCategories(_ calEvents: CalEvent) -> NSObject {
        var categoriesArray = [String]()
        if let categories = calEvents.category {
            for category in categories {
                categoriesArray.append(category.name ?? "")
            }
        }
        return categoriesArray as NSObject
    }
}
