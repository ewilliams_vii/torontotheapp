//
//  FestivalEventsViewModel.swift
//  Toronto
//
//  Created by Eric Williams on 2018-01-07.
//  Copyright © 2018 Eric Williams. All rights reserved.
//

import Foundation
import CoreLocation

class FestivalEventsViewModel {
    var calEvent: CalEvent
    var startDate: Date
    var startDateTime: String
    var allDay: Bool
    var endDateTime: String
    var coordinates: [CLLocation] = []
    
    init(calEvent: CalEvent, date: Dates) {
        self.calEvent = calEvent
        self.startDateTime = date.startDateTime ?? "--"
        self.allDay = date.allDay ?? false
        self.endDateTime = date.endDateTime ?? "--"
        self.startDate = date.startDateTime?.iso8601StringToDate ?? Date() 
    }
    
    init(calEvent: CalEvent, startDate: Date, startDateTime: String, allDay: Bool, endDateTime: String) {
        self.calEvent = calEvent
        self.startDate = startDate
        self.allDay = allDay
        self.endDateTime = endDateTime
        self.startDateTime = startDateTime
        self.coordinates = self.getCoordinates(from: calEvent) ?? []
    }
    
    fileprivate func getCoordinates(from calEvent: CalEvent) -> [CLLocation]? {
        var returnArray = [CLLocation]()
        if let locationArray = calEvent.locations {
            for location in locationArray {
                print(location)
//                returnArray.append(location)
            }
        }
        return returnArray
    }
}
