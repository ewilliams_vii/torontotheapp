//
//  EventHeaderTableViewCell.swift
//  Toronto
//
//  Created by Eric Williams on 2018-01-10.
//  Copyright © 2018 Eric Williams. All rights reserved.
//

import UIKit

class EventHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var sectionHeaderTitle: UILabel!
    @IBOutlet weak var seeMoreButton: UIButton!
    
    var view: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func draw(_ rect: CGRect) {
        // Drawing Code
    }
    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        commonInit()
//    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    fileprivate func commonInit() {
        view = loadViewFromXib()
        view.frame = bounds
        view.backgroundColor = UIColor.groupTableViewBackground
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(view)
        
        setNeedsDisplay()
        sectionHeaderTitle.text = "Test"
        
    }
    
    private func loadViewFromXib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let xib = UINib(nibName: "EventHeaderTableViewCell", bundle: bundle)
        let view = xib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    fileprivate func updateView() {
        //        print("EventHeaderTableViewCell > updateView")
    }
    
}
