//
//  FeaturedCollectionViewCell.swift
//  Toronto
//
//  Created by Eric Williams on 2018-01-08.
//  Copyright © 2018 Eric Williams. All rights reserved.
//

import UIKit

class FeaturedCollectionViewCell: UICollectionViewCell {

    public var featuredEventViewModel: FestivalEventsViewModel?
    
    @IBOutlet weak var featureImageView: UIImageView!
    @IBOutlet weak var featureName: UILabel!
    @IBOutlet weak var featureDate: UILabel!
    @IBOutlet weak var featureShortDescription: UILabel!
    
    var view: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func draw(_ rect: CGRect) {
        // Drawing Code
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    fileprivate func commonInit() {
        view = loadViewFromXib()
        view.frame = bounds
        view.backgroundColor = UIColor.groupTableViewBackground
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(view)
        
        setNeedsDisplay()
        featureName.text = "Test"
        featureShortDescription.text = "Lorem Ipsim. Deus solo veritas semper e unum. Dolor, wincet omnia."
    }
    
    private func loadViewFromXib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let xib = UINib(nibName: "FeaturedCollectionViewCell", bundle: bundle)
        let view = xib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    
    func updateView() {
        let featureEventImagePath = featuredEventViewModel?.calEvent.image?.url ?? ""
        self.featureImageView.image = nil
        if let featureEventImageUrl = BaseUrl.torontoFestivalEventsImagesEndpoint?.appendingPathComponent(featureEventImagePath) {
            self.featureImageView.downloadImage(for: featureEventImageUrl, contentMode: UIViewContentMode.scaleAspectFill)
        }
        
        let decodedEventName = String(htmlEncodedString: featuredEventViewModel?.calEvent.eventName ?? "-")
        self.featureName.text = decodedEventName?.capitalized
        let decodedShortDescription = String(htmlEncodedString: featuredEventViewModel?.calEvent.shortDescription ?? "-")
        self.featureShortDescription.text = decodedShortDescription
        
        if let startDateTime = featuredEventViewModel?.calEvent.dates?.first?.startDateTime!.iso8601StringToDate,
            let endDateTime = featuredEventViewModel?.calEvent.dates?.last?.endDateTime?.iso8601StringToDate, let startDateString = featuredEventViewModel?.startDateTime, let endDateSring = featuredEventViewModel?.endDateTime {
            self.featureDate.text = Calendar.current.isDate(startDateTime, inSameDayAs: endDateTime) ? startDateString : startDateString + " to " + endDateSring
        } else {
            self.featureDate.text = "--"
        }
        self.featureDate.textColor = .gray
        self.isSelected = false
    } 
}
