//
//  ViewController.swift
//  Toronto
//
//  Created by Eric Williams on 2017-12-31.
//  Copyright © 2017 Eric Williams. All rights reserved.
//

import UIKit
import CoreData

struct Objects {
    var sectionName: String!
    var sectionObjects: [FestivalEventsViewModel]!
}

extension Objects: Hashable {
    static func == (lhs: Objects, rhs: Objects) -> Bool {
        return lhs.sectionName == rhs.sectionName
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(sectionName)
    }
}

class FestivalEventsController: BaseViewController {
    
    typealias EventDate = String

    // IBOutlets 
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: BaseTableView!
    
    @IBOutlet weak var featuredEventsLabel: UILabel!
    @IBOutlet weak var FeaturedEventsCollectionView: UICollectionView!
    
    // DataSource
    var allEventsArray1 = [FestivalEventsViewModel]()
    var allEventsArray = [FestivalEventsViewModel]()
    
    var featuredEventsArray = [FestivalEventModel]()
    var featuredEventsViewModelArray = [FestivalEventsViewModel]()
    
    var allEventsDictionary = [EventDate: [FestivalEventsViewModel]]()
    var objectSet: Set = Set<Objects>()
    var objectArray = [Objects]()
    
    var events = [Event]()
    
    // ViewController Setup 
    fileprivate func setupTableViewData() {
        events.removeAll()
        let context = PersistenceService.sharedInstance.context
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Event")
        
        do {
            let results = try context.fetch(fetchRequest)
            let eventsArray = results as! [Event]
            for event in eventsArray {
                self.events.append(event)
            }
            tableView.reloadData()
        } catch {
            print(error)
        }
        tableView.reloadData()
        
    }
    
    fileprivate func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.allowsSelection = true
        collectionView.allowsMultipleSelection = true
        
        collectionView.translatesAutoresizingMaskIntoConstraints =  false
        collectionView.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height / 2)
        collectionView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectionView.topAnchor.constraint(equalTo: featuredEventsLabel.topAnchor, constant: 120)
        collectionView.heightAnchor.constraint(equalToConstant: 240.0).isActive = true
        collectionView.widthAnchor.constraint(equalToConstant: view.bounds.width).isActive = true
    }
    
    fileprivate func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.layoutMargins = UIEdgeInsets.zero
        tableView.separatorInset = UIEdgeInsets.zero
        //        tableView.separatorStyle = .none
        
        tableView.translatesAutoresizingMaskIntoConstraints =  false
        tableView.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height / 2)
        tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.heightAnchor.constraint(equalToConstant: view.bounds.height / 2).isActive = true
        tableView.widthAnchor.constraint(equalToConstant: view.bounds.width).isActive = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableViewData()
        setupCollectionView()
        setupTableView()
    }
    
    fileprivate func presentFestivalEventsDetails(for event: FestivalEventsViewModel) {
        let storyboard = UIStoryboard(name: "FestivalEvents", bundle: Bundle.main)
        let festivalEventsDetailVC = storyboard.instantiateViewController(withIdentifier: "FestivalEventsDetailViewController") as! FestivalEventsDetailViewController
        let festivalEventsViewModel = FestivalEventsViewModel(calEvent: event.calEvent,
                                                              startDate: Date(),
                                                              startDateTime: "",
                                                              allDay: false,
                                                              endDateTime: "")
        festivalEventsDetailVC.festivalEventViewModel = festivalEventsViewModel
        self.present(festivalEventsDetailVC, animated: true, completion: nil)
    }
}

extension FestivalEventsController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return featuredEventsViewModelArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "featuredEventCell", for: indexPath) as? FeaturedCollectionViewCell else {  return UICollectionViewCell() }
        let featuredEventViewModel = featuredEventsViewModelArray[indexPath.row]
        cell.featuredEventViewModel = featuredEventViewModel
        cell.updateView()
        return cell
    }
}

extension FestivalEventsController: UICollectionViewDelegate {
 
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let event = featuredEventsViewModelArray[indexPath.row]
//        guard let calEvent = event.calEvent else { return }
//        let festivalEventsViewModel = FestivalEventsViewModel(calEvent: calEvent, date: nil)
        let festivalEventsViewModel = FestivalEventsViewModel(calEvent: event.calEvent,
                                                              startDate: Date(),
                                                              startDateTime: "",
                                                              allDay: false,
                                                              endDateTime: "")
        presentFestivalEventsDetails(for: festivalEventsViewModel)
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
}

extension FestivalEventsController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell") as? EventTableViewCell else { return EventTableViewCell() }
 
        let event = objectArray[indexPath.section].sectionObjects[indexPath.row]
        guard let eventName = event.calEvent.eventName else { return UITableViewCell() }
        
        let decodedEventName = String(htmlEncodedString: eventName)
        cell.eventName.text = decodedEventName 
        
        if let shortDescription = event.calEvent.shortDescription, let description = event.calEvent.description {
            if shortDescription != "" {
                let decodedShortDescription = String(htmlEncodedString: shortDescription)
                cell.shortDescription.text = decodedShortDescription
            } else if description != "" {
                let decodedDescription = String(htmlEncodedString: description)
                cell.shortDescription.text = decodedDescription
            } else {
                cell.shortDescription.text = ""
                cell.shortDescription.isHidden = true
            }
        }
        return cell
    }
}

extension FestivalEventsController: UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.objectArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectArray[section].sectionObjects.count
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let attributes: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12, weight: .bold),
                                                        NSAttributedStringKey.backgroundColor: UIColor.darkGray,
                                                        NSAttributedStringKey.foregroundColor : UIColor.white 
        ]
        
        let text = "  " + self.objectArray[section].sectionName + "   "
        let label = SectionHeadLabel()
        label.attributedText = NSAttributedString(string: text, attributes: attributes)
        return label
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let event = objectArray[indexPath.section].sectionObjects[indexPath.row]
        presentFestivalEventsDetails(for: event)
    }
}

extension FestivalEventsController {
    func getDataFromUrl(url: URL, completion:@escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, oops in
            completion(data, response, oops)
        }.resume()
    }
    
    func downloadImage(url: URL, imageView: UIImageView) {
        getDataFromUrl(url: url) { (data, response, oops) in
            guard let data = data, oops == nil else { return }
            DispatchQueue.main.async(execute: {
                imageView.contentMode = .scaleAspectFit
                imageView.image = UIImage(data: data)
            })
        }
    }
}

extension FestivalEventsController {
    fileprivate func getEventsDataFromRemoteAPI() {
        Networking.sharedInstance.fetchTorontoFestivalsEvents(success: { festivalEvent in
            _ = festivalEvent.map {
                if let calEvent = $0.calEvent {
                    print("calEvent: \(calEvent)")
                    let hasImage = calEvent.image?.url !=  nil
                    let hasShortDescription = calEvent.shortDescription != nil
                    var isNotOver: Bool = true
                    if let overallEndDate = calEvent.endDate?.iso8601StringToDate {
                        isNotOver = overallEndDate > Date()
                    }
                    if isNotOver {
                        // Add all Festival Events to: allEventsArray
                        
                        if let calEvent = $0.calEvent, let calEventDatesArray = $0.calEvent?.dates {
                            for date in calEventDatesArray {
                                if let startDateTime = date.startDateTime?.iso8601StringToDate {
                                    let key: EventDate = startDateTime.screenReadableShort
                                    if self.allEventsDictionary.keys.contains(key) {
                                        self.allEventsDictionary[key]?.append(FestivalEventsViewModel(calEvent: calEvent, date: date))
                                    } else {
                                        
                                        self.allEventsDictionary[key] = []
                                        self.allEventsDictionary[key]?.append(FestivalEventsViewModel(calEvent: calEvent, date: date))
                                    }
                                }
                            }
                        }
                        // Add only Festival Events with Images to: featuredEventsArray
                        if hasImage && hasShortDescription {
                            self.featuredEventsArray.append($0)
                            if let calEvent = $0.calEvent, let startDate = $0.calEvent?.dates?.first?.startDateTime?.iso8601StringToDate, let startDateTime = $0.calEvent?.startDateTime?.iso8601StringToDate?.screenReadableShort, let allDay = $0.calEvent?.allDay, let endDateTime = $0.calEvent?.dates?.last?.endDateTime?.iso8601StringToDate?.screenReadableShort {
                                let featuredEventsViewModel = FestivalEventsViewModel(calEvent: calEvent,
                                                                                      startDate: startDate,
                                                                                      startDateTime: startDateTime,
                                                                                      allDay: allDay,
                                                                                      endDateTime: endDateTime)
                                self.featuredEventsViewModelArray.append(featuredEventsViewModel)
                            }
                        }
                    }
                }
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy"
            
            for (key,value) in self.allEventsDictionary {
                self.objectArray.append(Objects(sectionName: key, sectionObjects: value))
            }
            
            self.objectArray = self.objectArray.sorted(by: { dateFormatter.date(from: $0.sectionName)?.compare(dateFormatter.date(from: $1.sectionName) ?? Date()) == .orderedAscending })
            self.objectArray = self.objectArray.filter({ dateFormatter.date(from: $0.sectionName) ?? Date() > Date() })
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.collectionView.reloadData()
            }
            
        }) { (oops) in
            if let oops = oops {
                print(oops.localizedDescription)
            }
        }
    }
}
