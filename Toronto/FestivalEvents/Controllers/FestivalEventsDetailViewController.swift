		//
//  FestivalEventsDetailViewController.swift
//  Toronto
//
//  Created by Eric Williams on 2018-01-09.
//  Copyright © 2018 Eric Williams. All rights reserved.
//

import UIKit

class FestivalEventsDetailViewController: UIViewController {
    
    // IBOutlets
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var dismissViewController: UIButton!
    
    @IBOutlet weak var eventImageFrameView: UIView!
    @IBOutlet weak var eventImage: UIImageView!
    
    @IBOutlet weak var eventContentView: UIView!
    @IBOutlet weak var eventNameLabel: UILabel!
    @IBOutlet weak var eventDateLabel: UILabel!
    @IBOutlet weak var eventLocationLabel: UILabel!
    @IBOutlet weak var eventCostLabel: UILabel!
    @IBOutlet weak var eventDescriptionLabel: UILabel!
    @IBOutlet weak var eventDescriptionTextView: UITextView!
    @IBOutlet weak var eventWebsiteLabel: UILabel!
    
    @IBOutlet weak var eventsActionView: UIView!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var addToCalendarButton: UIButton!
    @IBOutlet weak var phoneEventButton: UIButton!
    
    // DataSource
    public var festivalEventViewModel: FestivalEventsViewModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        scrollView.contentInset = UIEdgeInsets.zero
        scrollView.contentSize = CGSize(width: self.view.bounds.size.width, height: 100.0)
        
        guard let festivalEventViewModel = festivalEventViewModel else { return }
        guard let featureEventImagePath = festivalEventViewModel.calEvent.image?.url else { return }
        if let featureEventImageUrl = BaseUrl.torontoFestivalEventsImagesEndpoint?.appendingPathComponent(featureEventImagePath) {
            eventImage.downloadImage(for: featureEventImageUrl, contentMode: UIViewContentMode.scaleAspectFill)
            print("eventImage.bounds.size \(eventImage.bounds.size)")
        }
        
//        guard let calEvent = festivalEventViewModel.calEvent else { return }
        
        let eventName = festivalEventViewModel.calEvent.eventName ?? ""
        let decodedEventName = String(htmlEncodedString: eventName)
        
        let attributes = [
            NSAttributedStringKey.foregroundColor : UIColor.darkGray,
            NSAttributedStringKey.font : UIFont(name: "ArialRoundedMTBold", size: 24)!
        ] as [NSAttributedStringKey : Any]
        
        eventNameLabel.attributedText = NSMutableAttributedString(string: decodedEventName!, attributes: attributes)
        
        let eventLocationName = festivalEventViewModel.calEvent.orgName
        let eventAddress = festivalEventViewModel.calEvent.locations?.first?.address
        eventLocationLabel.text = eventLocationName! + "\n" + (eventAddress ?? "--") 
        
        if let eventStartTime = festivalEventViewModel.calEvent.dates?.first?.startDateTime {
            if let eventEndTime = festivalEventViewModel.calEvent.dates?.first?.endDateTime {
                if let eventStartTime_isoDate = eventStartTime.iso8601StringToDate, let eventEndTime_isoDate = eventEndTime.iso8601StringToDate {
                    eventDateLabel.text = eventStartTime_isoDate.screenReadable + " - " + eventEndTime_isoDate.screenReadable
                }
            }
        }
        
        if let longDescription = festivalEventViewModel.calEvent.description {
            let decodedLogDescription = String(htmlEncodedString: longDescription)
            self.eventDescriptionTextView.text = decodedLogDescription
            self.eventDescriptionTextView.textContainer.heightTracksTextView = true
            self.eventDescriptionTextView.isScrollEnabled = false
        }
        
        eventCostLabel.text = "Festival Cost"
        
        if let webSiteUrl = festivalEventViewModel.calEvent.eventWebsite {
            eventWebsiteLabel.text = webSiteUrl
        }
        
    }

    @IBAction func dismissViewController(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
} 
