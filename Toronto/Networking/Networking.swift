//
//  AFNetworking.swift
//  Toronto
//
//  Created by Eric Williams on 2018-01-01.
//  Copyright © 2018 Eric Williams. All rights reserved.
//

import Foundation

enum EventDataResult {
    case success(Data)
    case failure(Error)
}

enum EventResult<T:Decodable> {
    case success([T])
    case failure(Error)
}

enum EventEndpoint: String {
    case torontoFestivalEvents = "http://app.toronto.ca/cc_sr_v1_app/data/edc_eventcal_APR?limit=500"
}

public class Networking {
    static let sharedInstance = Networking()
    private init () { }
    
    func fetchTorontoFestivalsEvents(success: @escaping([FestivalEventModel]) -> Void, failure:@escaping (Error?) -> Void) {
//        guard let tfestUrl = NetworkCall.torontoFestivalEvents_first20 else { return }
        guard let tfestUrl = NetworkCall.torontoFestivalEvents_first100 else { return }
//        guard let tfestUrl = NetworkCall.torontoFestivalEvents_first500 else { return }
        URLSession.shared.dataTask(with: tfestUrl) { (data, response, oops) in
            guard let data = data else { return }
            
            do {
                let decoder = JSONDecoder()
                let festivalEventData = try decoder.decode([FestivalEventModel].self, from: data)
                success(festivalEventData)
            } catch let oops {
                print("\n\n\nError\nAFNetworking.swift > fetchTorontoFestivalsEvents: ", oops)
                failure(oops)
            }
        }.resume()
    }
    
    func fetch(from entityEndpoint: EventEndpoint, completion: @escaping(EventDataResult) -> Void) {
        guard let urlEndpoint = URL(string: entityEndpoint.rawValue) else { return }
        URLSession.shared.dataTask(with: urlEndpoint) { (data, response, error) in
            if let error = error {
                completion(.failure(error))
            }
            
            guard let data = data else { return }
            completion(.success(data))
        }
        
    }
    
    func fetch<T: Decodable>(entityClass: T, from entityEndpoint: EventEndpoint, completion: @escaping(EventResult<T>) -> Void) {
        guard let urlEndpoint = URL(string: entityEndpoint.rawValue) else { return }
        URLSession.shared.dataTask(with: urlEndpoint) { (data, response, error) in
            guard let data = data else { return }
            
            self.decode(data: data, to: entityClass, completion: { (eventResult) in
                switch eventResult {
                    
                case .success(_):
                    print("success")
                case .failure(_):
                    print("failure")
                }
            })
            
//            do {
//                let decoder = JSONDecoder()
//                let eventData = try decoder.decode([T].self, from: data)
//                completion(.success([eventData]))
//            } catch let error {
//                completion(.failure(error))
//            }
        }
    }

    private func decode<T: Decodable>(data: Data, to: T, completion: @escaping(EventResult<T>) -> Void) {
        do {
            let decoder = JSONDecoder()
            let eventData = try decoder.decode([T].self, from: data)
//            completion(.success([eventData]))
            
        } catch let error {
            completion(.failure(error))
        }
    }
}

public class EventsSync {
    init<T: Decodable>(entityEndpoing: EventEndpoint, entityClass: T.Type) {
        Networking.sharedInstance.fetch(from: .torontoFestivalEvents) { (eventDataResult) in
            switch eventDataResult {
            case .success(let data):
                self.decode(data: data, to: entityClass, completion: { (eventReults) in
                    switch eventReults {
                        
                    case .success(let result):
                        self.makeEventModel(from: result, of: T.self)
                    case .failure(_):
                        print("failed")
                    }
                })
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func decode<T: Decodable>(data: Data, to: T.Type, completion: @escaping(EventResult<T>) -> Void) {
        do {
            let decoder = JSONDecoder()
            let eventData = try decoder.decode([T].self, from: data)
//            completion(.success([eventData]))
        } catch let error {
            completion(.failure(error))
        }
    }
    
    private func makeEventModel<T: Decodable>(from array: [T], of entityClass: T.Type) {
        if T.self == FestivalEventModel.self {
            print("\n\n\n\narray: \(array)\n\n\n\n\n\n")
        }
    }
    
 /*   private func convertFestivalEventModelToEventModel(_ array: [FestivalEventModel]) -> [Event] {
        for obj in array {
            let event = Event(context: PersistenceService.sharedInstance.context)
            event.id = UUID().uuidString
            event.idRemote = obj.calEvent?.eventName
            event.name = obj.calEvent?.eventName
            event.date = obj.calEvent?.startDate
            event.startDate = obj.calEvent?.startDate
            event.endDate = obj.calEvent?.endDate
            event.allDates = obj.calEvent?.dates
            event.locations = obj.calEvent?.locations
//            event.sharedWith = obj.calEvent.
            event.locationName = obj.calEvent?.locations?.first?.locationName
            event.address = obj.calEvent?.locations?.first?.address
            event.website = obj.calEvent?.eventWebsite
            event.locationType = obj.calEvent?.locations?.first?.locationType
            event.reservationPhone = obj.calEvent?.eventPhone
            event.descriptionShort = obj.calEvent?.shortDescription
            event.organizers = obj.calEvent?.orgName
            event.imageURI = obj.calEvent?.image?.url
            event.imageName = obj.calEvent?.image?.fileName
            event.imageFileType = obj.calEvent?.image?.fileType
            event.imageFileSize = obj.calEvent?.image?.fileSize
            event.imageAltText = obj.calEvent?.image?.altText
            event.imageHeight = 100
            event.imageWidth = 100
//            event.admissionCosts = obj.
//            event.admissionPrice = obj.
            event.categories = obj.calEvent?.category
            event.isFree = false
            event.isFeatured = obj.
//            event.reservationEmail = obj.calEvent.
            event.reservationWebsite = ""
            event.isSaved = false
            event.isCancelled = false
            event.isOutdoors = false
            event.isRescheduled = false
//            event.rescheduledDate = ""
            event.startTime = obj.calEvent?.startDateTime
            event.endTime = obj.calEvent?.endDateTime
            event.isAllDay = false
//            event.thumbnailName = obj.
//            event.thumbnailURI = obj.
//            event.thumbnailAltText = obj.
//            event.thumbnailWidth = obj.
//            event.thumbnailHeight = obj.
//            event.thumbnailFileType = obj.
//            event.thumbnailFileSize = obj.
        }
    }*/
}
