//
//  RootCoordinator.swift
//  Toronto
//
//  Created by Eric Williams on 2018-11-09.
//  Copyright © 2018 Eric Williams. All rights reserved.
//

import Foundation

public class Coordinator { }

protocol AppCoordinatorDelegate {
    func presentNotification(message: String)
}

public class RootCoordinator: NSObject {
    fileprivate var childCoordinators = [Coordinator]()
    
    var navigationController: AppNavigationController
    var delegate: AppCoordinatorDelegate?
    
    public init(with navigationController: AppNavigationController) {
        self.navigationController = navigationController
    }
    
    public func start() {
        setupNotificationObservers()
        showHomeFeedViewController()
    }

    deinit {
        nc.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        nc.removeObserver(self, name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        nc.removeObserver(self, name: NSNotification.Name.UIApplicationDidFinishLaunching, object: nil)
        nc.removeObserver(self, name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        nc.removeObserver(self, name: NSNotification.Name.UIApplicationWillTerminate, object: nil)
    }
    
    fileprivate func showHomeFeedViewController() {
        let homefeed = HomeFeedViewController(nibName: "HomeFeedViewController", bundle: Constants.bundle)
        homefeed.homeFeedViewControllerDelegate = self as? HomeFeedViewControllerDelegate
        homefeed.updateHomeFeedViewController()
        navigationController.show(homefeed, sender: self)
    }
}

extension RootCoordinator {
    fileprivate func setupNotificationObservers() {
        nc.addObserver(self, selector: #selector(didFinishLaunching), name: NSNotification.Name.UIApplicationDidFinishLaunching, object: nil)
        nc.addObserver(self, selector: #selector(willResignActive), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        nc.addObserver(self, selector: #selector(didEnterBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        nc.addObserver(self, selector: #selector(willEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        nc.addObserver(self, selector: #selector(didBecomeActive), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        nc.addObserver(self, selector: #selector(willTerminate), name: NSNotification.Name.UIApplicationWillTerminate, object: nil)
    }
    
    // MARK: - Manage App State
    @objc fileprivate func didFinishLaunching() { }
    @objc fileprivate func willResignActive() { }
    @objc fileprivate func didEnterBackground() { }
    @objc fileprivate func willEnterForeground() { }
    @objc fileprivate func didBecomeActive() { }
    @objc fileprivate func willTerminate() { }
}
