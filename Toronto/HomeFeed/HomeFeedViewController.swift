//
//  HomeFeedViewController.swift
//  Toronto
//
//  Created by Eric Williams on 2018-11-09.
//  Copyright © 2018 Eric Williams. All rights reserved.
//

import UIKit

protocol HomeFeedViewControllerDelegate: class {
    func showAccountManagement()
}

class HomeFeedViewController: BaseViewController {

    weak var homeFeedViewControllerDelegate: HomeFeedViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @objc func updateHomeFeedViewController() {
        
    }

}
